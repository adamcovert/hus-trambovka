$(document).ready(function () {

  /**
   * Show and close mobile menu
   */
  $('.s-hamburger').on('click', function () {
    $('.s-mobile-menu').addClass('s-mobile-menu--is-active');
    $('body').addClass('s-prevent-scroll');
  });

  $('.s-mobile-menu__close').on('click', function () {
    $('.s-mobile-menu').removeClass('s-mobile-menu--is-active');
    $('body').removeClass('s-prevent-scroll');
  });



  /**
   * Swiper sliders
   */
  var mySwiper = new Swiper('.s-promo__slider .swiper-container', {
    speed: 750,
    loop: true,
    parallax: true,
    centeredSlides: true,
    autoplay: true,
    // navigation: {
    //   nextEl: '.s-promo__slider-nav-btn--next',
    //   prevEl: '.s-promo__slider-nav-btn--prev',
    // },
    on: {
      init: function () {
        let swiper = this;
        for (let i = 0; i < swiper.slides.length; i++) {
          $(swiper.slides[i])
            .find('.s-promo__slide-cover')
            .attr({
              'data-swiper-parallax': 0.875 * swiper.width,
              'data-swiper-paralalx-opacity': 0.25
            });
          $(swiper.slides[i])
            .find('.s-promo__slide-title')
            .attr('data-swiper-parallax', 0.65 * swiper.width);
          $(swiper.slides[i])
            .find('.s-promo__slide-desc')
            .attr('data-swiper-parallax', 0.65 * swiper.width);
          $(swiper.slides[i])
            .find('.s-promo__get-more')
            .attr('data-swiper-parallax', 0.65 * swiper.width);
        }
      },
      resize: function () {
        this.update();
      }
    }
  })

  $('.s-offer__show-more-btn').on('click', function () {
    var button = $(this);
    var text = button.text() == 'Показать больше' ? 'Показать меньше' : 'Показать больше';
    button.text(text);
  });
});